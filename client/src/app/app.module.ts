import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './app-material.module';
import { UploadDialogComponent } from './common/components/upload-dialog/upload-dialog.component';
import { UpdateDialogComponent } from './common/components/update-dialog/update-dialog.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientModule } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { VideoComponent } from './common/components/video/video.component';

@NgModule({
	declarations: [AppComponent, UploadDialogComponent, UpdateDialogComponent, VideoComponent],
	imports: [
		BrowserModule,
		CommonModule,
		HttpClientModule,
		FormsModule,
		AppRoutingModule,
		AppMaterialModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		MatSnackBarModule,
		MatProgressSpinnerModule,
		MatFormFieldModule,
		MatExpansionModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {}
