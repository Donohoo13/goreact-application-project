import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
	mergeMap,
	Observable,
	of,
	scan,
	shareReplay,
	startWith,
	Subject,
	takeWhile,
	tap
} from 'rxjs';
import { UploadDialogComponent } from './common/components/upload-dialog/upload-dialog.component';
import { UploadService } from './common/services/upload/upload.service';
import { UserVideo } from './common/types/user-video';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
	@ViewChild(MatAccordion) accordion!: MatAccordion;
	videos$: Observable<UserVideo[]> = of([]);
	openVideoId$: Subject<String> = new Subject<String>();
	upload$: Subject<UserVideo> = new Subject<UserVideo>();

	constructor(
		private dialog: MatDialog,
		private snackbar: MatSnackBar,
		private upload: UploadService
	) {}

	ngOnInit(): void {
		this.refreshVideos();
	}

	refreshVideos() {
		this.videos$ = this.upload.getVideos().pipe(
			// Transforms the cold observable to a hot observable, meaning we only do the network request once
			shareReplay(),

			// Merge that result with any new videos that the user uploads
			mergeMap((existingVideos: UserVideo[]) => {
				return this.upload$.pipe(
					// Scan is a version of reduce() that emits each update
					scan((videos, newUpload) => videos.concat(newUpload), existingVideos),
					startWith(existingVideos)
				);
			})
		);
	}

	openUpload() {
		const dialogRef = this.dialog.open<UploadDialogComponent, any, UserVideo>(
			UploadDialogComponent
		);
		dialogRef
			.afterClosed()
			.pipe(
				// Log out what they did
				tap((result: UserVideo | undefined) => {
					console.log('Dialog result:', result ?? 'cancelled');
				}),

				/*
          I believe this could be improved by using the takeWhile instead
          The filter here simply ignores the result if it is either undefined or null
          The danger with this is that it will still leave the subscription hanging
          takeWhile on the other hand will actually fire complete if the condition is not met

          filter((result: UserVideo | undefined): result is UserVideo => result != null),
				 */

				takeWhile(
					// Typescript syntax to create a type predicate for the given block
					// Otherwise it's a straight forward ES6 one liner method for whether or not result is null or undefined
					(result: UserVideo | undefined): result is UserVideo => result != null
				),

				// Show success message
				tap(() => {
					this.snackbar.open('Video uploaded successfully', undefined, {
						duration: 4000
					});
				})

				// No need to unsubscribe because afterClosed emits complete that will clean up this subscription
			)
			.subscribe((result: UserVideo) => this.upload$.next(result));
	}

	triggerAccordion(accordionId: String) {
		this.openVideoId$.next(accordionId);
	}
}
