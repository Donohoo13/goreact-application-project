export interface ApiRes<T> {
	data: T;
}
