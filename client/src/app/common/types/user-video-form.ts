export interface UserVideoForm {
	id: number;
	video: File;
	title: string;
	description: string;
	tags: string;
}
