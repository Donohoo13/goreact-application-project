import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { UploadService } from '../../services/upload/upload.service';
import { ReactiveFormsModule } from '@angular/forms';
import { VideoComponent } from './video.component';
import { UpdateDialogComponent } from '../update-dialog/update-dialog.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';

describe('VideoComponent', () => {
	let component: VideoComponent;
	let fixture: ComponentFixture<VideoComponent>;
	let uploadService: UploadService;
	let dialogMock!: jasmine.SpyObj<MatDialog>;
	let snackbarMock!: jasmine.SpyObj<MatSnackBar>;

	beforeEach(async () => {
		dialogMock = jasmine.createSpyObj(['open']);
		snackbarMock = jasmine.createSpyObj(['open']);
		await TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				MatDialogModule,
				MatIconModule,
				MatSnackBarModule,
				MatExpansionModule,
				ReactiveFormsModule,
				BrowserAnimationsModule
			],
			declarations: [VideoComponent],
			providers: [
				{ provide: MatDialog, useValue: dialogMock },
				{ provide: MatSnackBar, useValue: snackbarMock }
			]
		}).compileComponents();

		uploadService = TestBed.inject(UploadService);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(VideoComponent);
		component = fixture.componentInstance;
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	describe('upload', () => {
		it('should show snackbar if update is completed', fakeAsync(() => {
			// Simulate success on modal
			const dialogRefMock = jasmine.createSpyObj(['afterClosed']);
			dialogRefMock.afterClosed.and.returnValue(
				of({ id: 1, path: 'bogus/path', title: 'Awesome', description: 'test', tags: '' })
			);
			dialogMock.open.and.returnValue(dialogRefMock);

			// Simulate the user pressing the upload button
			component.openUpdateVideo();

			// Should see a snackbar if the user's upload was successful
			expect(snackbarMock.open).toHaveBeenCalled();
		}));

		it('should not show snackbar if upload is cancelled', fakeAsync(() => {
			// Simulate cancel on modal
			const dialogRefMock = jasmine.createSpyObj(['afterClosed']);
			dialogRefMock.afterClosed.and.returnValue(of(undefined));
			dialogMock.open.and.returnValue(dialogRefMock);

			// Simulate the user pressing the upload button
			component.openUpdateVideo();

			// Shouldn't see a snackbar if the user canceled
			expect(snackbarMock.open).not.toHaveBeenCalled();
		}));
	});
});
