import {
	Component,
	OnInit,
	ChangeDetectionStrategy,
	Input,
	Output,
	EventEmitter,
	ViewChild
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, Subject, takeWhile, tap } from 'rxjs';
import { UserVideo } from '../../types/user-video';
import { UpdateDialogComponent } from '../update-dialog/update-dialog.component';

@Component({
	selector: 'app-video',
	templateUrl: './video.component.html',
	styleUrls: ['./video.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class VideoComponent implements OnInit {
	@ViewChild(MatAccordion) accordion!: MatAccordion;
	@Input() video!: UserVideo;
	@Input() openVideoId$!: Observable<String>;
	@Output() triggerAccordion: EventEmitter<String> = new EventEmitter();
	updateVideo$: Subject<UserVideo> = new Subject<UserVideo>();

	constructor(private dialog: MatDialog, private snackbar: MatSnackBar) {}

	ngOnInit(): void {
		// Close the accordion for this video if another accordion opens up
		this.openVideoId$.subscribe((val) => {
			if (val !== this.accordion.id) {
				this.accordion.closeAll();
			}
		});
	}

	openUpdateVideo() {
		const dialogRef = this.dialog.open<UpdateDialogComponent, UserVideo, UserVideo>(
			UpdateDialogComponent,
			{ data: this.video }
		);
		dialogRef
			.afterClosed()
			.pipe(
				// Log out what they did
				tap((result: UserVideo | undefined) => {
					console.log('Dialog result:', result ?? 'cancelled');
				}),

				takeWhile((result: UserVideo | undefined): result is UserVideo => result != null),

				// Show success message
				tap(() => {
					this.snackbar.open('Video uploaded successfully', undefined, {
						duration: 4000,
						panelClass: 'success-snackbar'
					});
				})

				// No need to unsubscribe because afterClosed emits complete that will clean up this subscription
			)
			.subscribe((result: UserVideo) => this.updateVideo$.next(result));
	}

	triggerAccordionClicked() {
		this.triggerAccordion.emit(this.accordion.id);
	}
}
