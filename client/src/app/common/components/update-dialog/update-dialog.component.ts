import { Component, ElementRef, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject } from 'rxjs';
import { UploadService } from '../../services/upload/upload.service';
import { UserVideo } from '../../types/user-video';
import { UserVideoForm } from '../../types/user-video-form';

@Component({
	selector: 'app-update-dialog',
	templateUrl: './update-dialog.component.html',
	styleUrls: ['./update-dialog.component.scss']
})
export class UpdateDialogComponent implements OnInit {
	isLoading: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);

	videoForm: FormGroup;
	idControl = new FormControl(null, [Validators.required]);
	titleControl = new FormControl(null, [Validators.required]);
	descriptionControl = new FormControl(null, [Validators.required]);
	tagsControl = new FormControl(null, [Validators.required]);

	constructor(
		private uploadService: UploadService,
		private dialogRef: MatDialogRef<UpdateDialogComponent, UserVideo>,
		@Inject(MAT_DIALOG_DATA) public dialogData: UserVideo,
		private snackbar: MatSnackBar,
		private fb: FormBuilder
	) {
		this.videoForm = fb.group({
			title: this.titleControl,
			description: this.descriptionControl,
			tags: this.tagsControl
		});

		if (dialogData != null) {
			this.videoForm.setValue({
				title: dialogData.title,
				description: dialogData.description,
				tags: dialogData.tags
			});
		}
	}

	ngOnInit(): void {}

	update() {
		if (this.videoForm.status === 'VALID') {
			// Trigger our loading state
			this.isLoading.next(true);
			const formVals = this.videoForm.value as UserVideoForm;

			// Attempt the update
			this.uploadService
				.update({ ...formVals, id: this.dialogData.id })
				.subscribe((result: UserVideo) => {
					this.isLoading.next(false);
					this.dialogRef.close(result);
				});
		}
	}

	getErrorMessage(control: FormControl) {
		if (control.hasError('required')) {
			return 'You must enter a value';
		}
		return null;
	}
}
