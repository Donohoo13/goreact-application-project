import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { UpdateDialogComponent } from './update-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { UploadService } from '../../services/upload/upload.service';
import { UserVideo } from '../../types/user-video';
import { of } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';

describe('UpdateDialogComponent', () => {
	let component: UpdateDialogComponent;
	let fixture: ComponentFixture<UpdateDialogComponent>;
	let uploadService: UploadService;
	let dialogRefMock!: jasmine.SpyObj<MatDialogRef<UpdateDialogComponent, any>>;

	beforeEach(async () => {
		dialogRefMock = jasmine.createSpyObj(['close']);
		await TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				MatDialogModule,
				MatIconModule,
				MatSnackBarModule,
				ReactiveFormsModule
			],
			declarations: [UpdateDialogComponent],
			providers: [
				{ provide: MatDialogRef, useValue: dialogRefMock },
				{
					provide: MAT_DIALOG_DATA,
					useValue: { id: '1', title: 'Party', description: 'nice', tags: 'brilliant' }
				}
			]
		}).compileComponents();

		uploadService = TestBed.inject(UploadService);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(UpdateDialogComponent);
		component = fixture.componentInstance;
	});

	describe('init', () => {
		it('should create', () => {
			expect(component).toBeTruthy();
		});
		it('should default the form to valid', () => {
			fixture.detectChanges();
			component.update();

			expect(component.videoForm.status === 'VALID');
		});
	});

	describe('update', () => {
		it('should call the upload.service update method, close the dialog when finished, if form is valid', () => {
			spyOn(uploadService, 'update').and.returnValue(
				of({
					id: 1,
					path: 'bogus/path',
					title: 'Awesome',
					description: 'test',
					tags: 'awesome'
				} as UserVideo)
			);
			fixture.detectChanges();

			component.update();

			expect(component.videoForm.status === 'VALID');
			expect(uploadService.update).toHaveBeenCalled();
			expect(dialogRefMock.close).toHaveBeenCalled();
		});

		it('should do nothing when calling the upload.service update method if not valid', () => {
			component.titleControl.setValue('');
			spyOn(uploadService, 'update').and.returnValue(
				of({
					id: 1,
					path: 'bogus/path',
					title: 'Awesome',
					description: 'test',
					tags: ''
				} as UserVideo)
			);
			fixture.detectChanges();

			component.update();

			expect(component.videoForm.status === 'INVALID');
			expect(uploadService.update).not.toHaveBeenCalled();
			expect(dialogRefMock.close).not.toHaveBeenCalled();
		});

		it('should disable the submit button until form is valid', () => {
			const compiled = fixture.nativeElement as HTMLElement;
			const submitButton = compiled.querySelector(
				'.mat-dialog-actions #update-video'
			) as HTMLButtonElement;
			fixture.detectChanges();
			expect(submitButton?.textContent).toContain('Update');

			// Fill in the form
			component.videoForm.setValue({
				title: 'Awesome',
				description: 'test',
				tags: 'awesome'
			});
			fixture.detectChanges();

			// Should now be enabled
			expect(submitButton?.disabled).toBeFalsy();
		});
	});
});
