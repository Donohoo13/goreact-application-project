import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject } from 'rxjs';
import { UploadService } from '../../services/upload/upload.service';
import { UserVideo } from '../../types/user-video';
import { UserVideoForm } from '../../types/user-video-form';

@Component({
	selector: 'app-upload-dialog',
	templateUrl: './upload-dialog.component.html',
	styleUrls: ['./upload-dialog.component.scss']
})
export class UploadDialogComponent implements OnInit {
	@ViewChild('fileInput') fileInput!: ElementRef;
	isLoading: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);

	videoForm: FormGroup;
	fileControl = new FormControl(null, [Validators.required]);
	titleControl = new FormControl(null, [Validators.required]);
	descriptionControl = new FormControl(null, [Validators.required]);
	tagsControl = new FormControl(null, [Validators.required]);

	constructor(
		private uploadService: UploadService,
		private dialogRef: MatDialogRef<UploadDialogComponent, UserVideo>,
		private snackbar: MatSnackBar,
		private fb: FormBuilder
	) {
		this.videoForm = fb.group({
			video: this.fileControl,
			title: this.titleControl,
			description: this.descriptionControl,
			tags: this.tagsControl
		});
	}

	ngOnInit(): void {}

	selectFile() {
		this.fileInput.nativeElement.click();
	}

	fileChange(event: Event) {
		// Beautiful conditional chaining
		const files: FileList = (event as any).target?.files;

		// Gotta make sure we actually got the file
		if (!files.length) return;

		// We don't allow any files over 50MB
		const MAX_FILE_SIZE = 50;
		// The size is calculated in bytes, we want megabytes
		const fileSize = files[0].size / 1_000_000;
		if (fileSize > MAX_FILE_SIZE) {
			// Let the user know why they can't upload the given video
			this.snackbar.open('The file size cannot exceed 50 MB', undefined, {
				duration: 4000,
				panelClass: 'warning-snackbar'
			});
			return;
		}

		if (files.length > 0) {
			this.fileControl.setValue(files[0]);
		}
	}

	upload() {
		if (this.videoForm.status === 'VALID') {
			// Trigger our loading state
			this.isLoading.next(true);
			const formVals = this.videoForm.value as UserVideoForm;

			// Attempt the upload
			this.uploadService.upload(formVals).subscribe((result: UserVideo) => {
				this.isLoading.next(false);
				this.dialogRef.close(result);
			});
		}
	}

	getErrorMessage(control: FormControl) {
		if (control.hasError('required')) {
			return 'You must enter a value';
		}
		if (control.hasError('pattern')) {
			return 'Value must be a comma separated list';
		}
		return null;
	}
}
