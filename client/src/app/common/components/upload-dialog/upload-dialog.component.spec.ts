import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { UploadDialogComponent } from './upload-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { UploadService } from '../../services/upload/upload.service';
import { UserVideo } from '../../types/user-video';
import { of } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';

describe('UploadDialogComponent', () => {
	let component: UploadDialogComponent;
	let fixture: ComponentFixture<UploadDialogComponent>;
	let uploadService: UploadService;
	let dialogRefMock!: jasmine.SpyObj<MatDialogRef<UploadDialogComponent, any>>;

	beforeEach(async () => {
		dialogRefMock = jasmine.createSpyObj(['close']);
		await TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				MatDialogModule,
				MatIconModule,
				MatSnackBarModule,
				ReactiveFormsModule
			],
			declarations: [UploadDialogComponent],
			providers: [{ provide: MatDialogRef, useValue: dialogRefMock }]
		}).compileComponents();

		uploadService = TestBed.inject(UploadService);
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(UploadDialogComponent);
		component = fixture.componentInstance;
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	describe('upload', () => {
		it('should call the upload.service upload method, close the dialog when finished, if a file was chosen', () => {
			component.videoForm.setValue({
				video: new File([''], 'bogusFile', { type: 'text/html' }),
				title: 'Awesome',
				description: 'test',
				tags: 'awesome'
			});
			spyOn(uploadService, 'upload').and.returnValue(
				of({
					id: 1,
					path: 'bogus/path',
					title: 'Awesome',
					description: 'test',
					tags: 'awesome'
				} as UserVideo)
			);
			fixture.detectChanges();

			component.upload();

			expect(uploadService.upload).toHaveBeenCalled();
			expect(dialogRefMock.close).toHaveBeenCalled();
		});

		it('should do nothing when calling the upload.service upload method if no file was chosen', () => {
			// component.file = null;
			spyOn(uploadService, 'upload').and.returnValue(
				of({
					id: 1,
					path: 'bogus/path',
					title: 'Awesome',
					description: 'test',
					tags: 'awesome'
				} as UserVideo)
			);
			fixture.detectChanges();

			component.upload();

			expect(uploadService.upload).not.toHaveBeenCalled();
			expect(dialogRefMock.close).not.toHaveBeenCalled();
		});

		it('should disable the submit button until a file has been selected', () => {
			const compiled = fixture.nativeElement as HTMLElement;
			const submitButton = compiled.querySelector(
				'.mat-dialog-actions #upload-video'
			) as HTMLButtonElement;
			// component.file = null;
			fixture.detectChanges();
			expect(submitButton?.textContent).toContain('Upload');

			// Fill in the form
			component.videoForm.setValue({
				video: new File([''], 'bogusFile', { type: 'text/html' }),
				title: 'Awesome',
				description: 'test',
				tags: 'awesome'
			});
			fixture.detectChanges();

			// Should now be enabled
			expect(submitButton?.disabled).toBeFalsy();
		});
	});
});
