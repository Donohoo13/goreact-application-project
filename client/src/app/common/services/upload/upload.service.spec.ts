import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { environment } from '../../../../environments/environment';
import { UploadService } from './upload.service';
import { UserVideoForm } from '../../types/user-video-form';

describe('UploadService', () => {
	let http: HttpTestingController;
	let service: UploadService;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			imports: [HttpClientTestingModule]
		});

		service = TestBed.inject(UploadService);
		http = TestBed.inject(HttpTestingController);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	describe('GET', () => {
		it('should call the correct endpoint when getting videos', () => {
			service.getVideos().subscribe((videos) => {
				expect(videos.length).toBe(1);
				expect(videos[0].id).toBe(1);
				expect(videos[0].path).toBe('bogus/path');
			});
			const req = http.expectOne(`${environment.apiBaseUrl}/videos`);
			expect(req.request.method).toBe('GET');
			// We now instead expect our api to return a data object containing our videos
			req.flush({ data: [{ id: 1, path: 'bogus/path' }] });
			http.verify();
		});

		it('should return an empty array if an error is thrown', () => {
			service.getVideos().subscribe((videos) => {
				expect(videos.length).toBe(0);
			});
			const req = http.expectOne(`${environment.apiBaseUrl}/videos`);
			expect(req.request.method).toBe('GET');
			// Make sure the request fails
			req.error(new ProgressEvent('Internal Server Error'));
			http.verify();
		});
	});

	describe('POST', () => {
		it('should call the correct endpoint when uploading a video', () => {
			const fileBlob = new File([''], 'bogusFile', { type: 'text/html' });
			const formData = {
				video: fileBlob,
				title: 'Awesome',
				description: 'test',
				tags: 'bueno'
			} as UserVideoForm;
			service.upload(formData).subscribe((video) => {
				expect(video.id).toBe(1);
				expect(video.path).toBe('bogus/path');
			});
			const req = http.expectOne(`${environment.apiBaseUrl}/videos`);
			expect(req.request.method).toBe('POST');
			// We now instead expect our api to return a data object containing our new video
			req.flush({ data: { id: 1, path: 'bogus/path' } });
			http.verify();
		});

		it('should return null if an error is thrown', () => {
			const fileBlob = new File([''], 'bogusFile', { type: 'text/html' });
			const formData = {
				video: fileBlob,
				title: 'Awesome',
				description: 'test',
				tags: 'bueno'
			} as UserVideoForm;
			service.upload(formData).subscribe((video) => {
				expect(video).toBeNull();
			});
			const req = http.expectOne(`${environment.apiBaseUrl}/videos`);
			expect(req.request.method).toBe('POST');
			// Make sure the request fails
			req.error(new ProgressEvent('Internal Server Error'));
			http.verify();
		});
	});

	describe('PATCH', () => {
		it('should call the correct endpoint when updating a video', () => {
			const id = 1;
			const formData = {
				id,
				title: 'Awesome',
				description: 'test',
				tags: 'bueno'
			} as UserVideoForm;
			service.update(formData).subscribe((video) => {
				expect(video.id).toBe(id);
				expect(video.path).toBe('bogus/path');
			});
			const req = http.expectOne(`${environment.apiBaseUrl}/videos/${id}`);
			expect(req.request.method).toBe('PATCH');
			// We now instead expect our api to return a data object containing our new video
			req.flush({
				data: {
					id: 1,
					path: 'bogus/path',
					title: 'Awesome',
					description: 'test',
					tags: 'bueno'
				}
			});
			http.verify();
		});

		it('should return null if an error is thrown', () => {
			const id = 1;
			const formData = {
				id,
				title: 'Awesome',
				description: 'test',
				tags: 'bueno'
			} as UserVideoForm;
			service.update(formData).subscribe((video) => {
				expect(video).toBeNull();
			});
			const req = http.expectOne(`${environment.apiBaseUrl}/videos/${id}`);
			expect(req.request.method).toBe('PATCH');
			// Make sure the request fails
			req.error(new ProgressEvent('Internal Server Error'));
			http.verify();
		});
	});
});
