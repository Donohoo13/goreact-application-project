import { ApiRes } from './../../types/api-response';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map, of } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { UserVideo } from '../../types/user-video';
import { UserVideoForm } from '../../types/user-video-form';

/**
 * As stated in my comments in api/tests/Feature/VideoControllerTest.php
 * this is one of the main places I needed to make changes to appease the "data" key in the response object.
 * Notice How I am now using a new type <ApiRes> which can be passed a generic type dictating what the data object should be.
 * This way we can still ensure the type is correct and usable down the chain
 * I also added error handlers within the service layer so as to not leave anything hanging
 */

@Injectable({
	providedIn: 'root'
})
export class UploadService {
	constructor(private http: HttpClient) {}

	getVideos(): Observable<UserVideo[]> {
		return this.http.get<ApiRes<UserVideo[]>>(`${environment.apiBaseUrl}/videos`).pipe(
			// We only care about the data object returned for now
			map((res) => {
				return res.data;
			}),
			catchError((err) => {
				console.error(err);
				// Fallback value to return on error
				return of([]);
			})
		);
	}

	upload(form: UserVideoForm): Observable<UserVideo> {
		const formData = new FormData();
		formData.append('video', form.video, form.video.name);
		formData.append('title', form.title);
		formData.append('description', form.description);
		formData.append('tags', form.tags);

		const headers = new HttpHeaders().set('Accept', 'application/json');

		return this.http
			.post<ApiRes<UserVideo>>(`${environment.apiBaseUrl}/videos`, formData, { headers })
			.pipe(
				// We only care about the data object returned for now
				map((res) => res.data),
				catchError((err) => {
					console.error(err);
					// Fallback value to return on error
					return of(null as any);
				})
			);
	}

	update(form: UserVideoForm): Observable<UserVideo> {
		const formData = new FormData();
		formData.append('title', form.title);
		formData.append('description', form.description);
		formData.append('tags', form.tags);

		const headers = new HttpHeaders().set('Accept', 'application/json');

		return this.http
			.patch<ApiRes<UserVideo>>(`${environment.apiBaseUrl}/videos/${form.id}`, formData, {
				headers
			})
			.pipe(
				// We only care about the data object returned for now
				map((res) => res.data),
				catchError((err) => {
					console.error(err);
					// Fallback value to return on error
					return of(null as any);
				})
			);
	}
}
