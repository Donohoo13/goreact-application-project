<?php

namespace App\Http\Controllers;

use App\Http\Resources\VideoResource;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    /**
     * Fetch a video
     *
     * @param  Video  $video
     * @return Response
     */
    public function getVideo(Video $video): VideoResource
    {
        return new VideoResource($video);
    }

    public function getAllVideos(): AnonymousResourceCollection
    {
        return VideoResource::collection(Video::all());
    }

    public function addVideo(Request $req): VideoResource
    {
        $path = Storage::putFile('storage', $req->file('video'));
        $title = $req->title;
        $description = $req->description;
        $tags = $req->tags;


        return VideoResource::make(Video::create([
            'path' => $path,
            'title' => $title,
            'description' => $description,
            'tags' => $tags
        ]));
    }

    public function updateVideo(Request $req, $id)
    {
        $video = Video::find($id);

        if ($req->has(['title', 'description', 'tags'])) {
            $video->title = $req->title;
            $video->description = $req->description;
            $video->tags = $req->tags;
        }

        // Save the changes and return the video
        $video->save();

        // return $video;
        return new VideoResource($video);
    }
}
