<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Video class
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $tags
 * @property string $path
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Video extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'videos';

    protected $fillable = ['title', 'description', 'tags', 'path'];
}
