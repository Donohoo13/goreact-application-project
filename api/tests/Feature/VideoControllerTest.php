<?php

namespace Tests\Feature;

use App\Models\Video;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Mockery;
use Tests\TestCase;

class VideoControllerTest extends TestCase
{
    /**
     * I ran into a huge issue with these tests and spent quite a bit of time battling the structure
     * I tried to attack this with the mindset of 'DO NOT TOUCH THE TEST FILES'
     * but eventually found this to be impossible unless I decide to slightly refactor the front end

     * The way each test expects a data key in the response is inconsistent with the angular project.
     * It's incredibly unclear as to what the expectation for what the response format should be.
     * Should an API call be returned with a 'data' object containing the results like it's written in the tests?
     * This approach in my opinion is best practice since it leaves us the ability to add more meta data to our api responses
    {
        data: [
            {id: 1, path: 'cool/path'},
            {id: 2, path: 'weird/path'}
        ],
        message: 'Successfully grabbed videos'
    };

     * Or is the intent to have each call simply return the information requested?
    [
        { id: 1, path: 'cool/path' },
        { id: 2, path: 'weird/path' }
    ]

     * I believe a good standard is to house result data if applicable into a parent 'data' object like the former as previously stated.
     * This gives the ability to add more helpful values into the response on top of just the data when desired.
    {
        "success": true,
        "message": "Successfully created a new video",
        "data": { }
    }

     * Because of this, it quickly became difficult to assert the projects intent while also trying to grasp PHP and Laravel at the same time.

     * I decided to stick with the integrity of the tests and change up the angular project.
     * This decision was made after quite a few hours of battling back and forth and trying to force things to work for me in a non intuitive way
     * I can discuss this troubleshooting process and my thoughts as I went a long in greater detail upon completion of this challenge
     */

    // $response->dump();
    // echo 'VIDEO :>> ' . json_encode($video);


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_videos_with_empty_db()
    {
        $response = $this->get('/api/videos');
        $response
            ->assertStatus(200)
            ->assertJsonPath('data', []);
    }

    public function test_add_video()
    {
        Storage::fake('public');
        $mp4 = UploadedFile::fake()->create('my-video.mp4', 1356, 'video/mp4');
        $response = $this->post('/api/videos', [
            'video' => $mp4,
            'title' => 'Wow',
            'description' => 'What a description!',
            'tags' => 'neato-burrito'
        ]);

        /** @var Video $video */
        $video = $response->baseResponse->original;

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'id', 'path', 'title', 'description', 'tags'
            ]
        ]);
        $this->assertNotNull($video);
        Storage::assertExists($video->path);
        Mockery::close();
    }

    public function test_get_videos()
    {
        Video::factory()->count(3)->create();
        $response = $this->get('/api/videos');
        $response
            ->assertStatus(200)
            ->assertJsonCount(3, 'data');
    }

    public function test_get_single_video()
    {
        $video = Video::factory()->create();
        $response = $this->get("/api/videos/{$video->getKey()}");
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id', 'path', 'title', 'description', 'tags'
                ]
            ]);
    }

    public function test_update_video()
    {
        $video = Video::factory()->create();
        $response = $this->patch('/api/videos/' . $video->getKey(), [
            'title' => 'Updated',
            'description' => 'Updated',
            'tags' => 'Updated'
        ]);

        /** @var Video $video */
        $video = $response->baseResponse->original;

        $response->assertStatus(200);
        $response
            ->assertJsonFragment(['title' => 'Updated', 'description' => 'Updated', 'tags' => 'Updated'])
            ->assertJsonStructure([
                'data' => [
                    'id', 'path', 'title', 'description', 'tags'
                ]
            ]);
        $this->assertNotNull($video);
    }
}
