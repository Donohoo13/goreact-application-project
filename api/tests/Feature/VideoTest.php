<?php

namespace Tests\Feature;

use App\Models\Video;
use Tests\TestCase;

class VideoTest extends TestCase
{
    public function test_create()
    {
        $path = 'storage/my-video.mp4';
        $title = 'My Video';
        $description = "Look mom, I'm in a video";
        $tags = 'lucky, #blessed';

        $video = new Video(['path' => $path, 'title' => $title, 'description' => $description, 'tags' => $tags]);
        $result = $video->save();
        $this->assertTrue($result);
    }
}
