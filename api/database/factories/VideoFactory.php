<?php

namespace Database\Factories;

use App\Models\Video;
use Illuminate\Database\Eloquent\Factories\Factory;

class VideoFactory extends Factory
{

    protected $model = Video::class;

    public function definition()
    {
        $id = $this->faker->numberBetween(1, 100);
        $name = $this->faker->name();

        return [
            'id' => $id,
            'path' => 'storage/video-' . $name . '.mp4',
            'title' => 'Video ' . $id,
            'description' => 'Video ' . $name,
            'tags' => 'cool, sweet, awesome'
        ];
    }
}
